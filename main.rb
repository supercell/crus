#!/usr/bin/env ruby
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
require "erb"
require "json"
require "net/http"
require "uri"
require "socket"

PORT = ENV['CRUS_PORT'] || 2787
API_URI = URI('https://kitsu.io/api/edge')

##
# returns the values of all name-value pairs whose name is `name`.
#
# `arr` is assumed to be an array in the form of [[name, value], [name, value]]
# that you get as a response from URI.decode_www_form.
def query_get_all(arr, name)
  values = Array.new

  arr.each do |pair|
    if pair[0] == name
      values.push(pair[1])
    end
  end

  return values
end

def render_erb(template_file, data = {})
  template = File.read("./templates/#{template_file}.erb")
  ERB.new(template).result(binding)
end

def parse_req(req)
  _req = req.split(" ")
  ret = Hash.new
  ret["method"] = _req[0]
  ret["path"] = _req[1]
  ret["http"] = _req[2]
  return ret
rescue NoMethodError
  ret = Hash.new
  ret["method"] = "GET"
  ret["path"] = "/"
  ret["http"] = "HTTP/1.1"
  return ret
end

# return the necessary lines for HTTP success.
def gen_http_header(status = 200, content_type = "text/html", cache = false)
  header = [
    "HTTP/1.1 #{status}",
    "Content-Type: #{content_type}",
    "Referrer-Policy: no-referrer"
  ]
  # 86,400 seconds = 1 day
  header.push("Cache-Control: private, max-age=86400, immutable") if cache
  return header.join("\r\n") + "\r\n\r\n"
end

def get_api(path, escape = true)
  # FIXME: Browse has filters which are a Hash converted to string with
  # URI.www_form_encode, URI.escape below will change the % into %25,
  # which is correct behaviour, except we don't need that. the issue
  # here is that I don't want the `if` statement and would rather use
  # URI.escape.
  if escape
    # use for search
    resource = URI.escape("#{API_URI}#{path}")
  else
    # used for most normal API requests
    resource = "#{API_URI}#{path}"
  end
  req = Net::HTTP::Get.new(resource)
  req["Accept"] = "application/vnd.api+json"
  req["Content-Type"] = "application/vnd.api+json"
  res = Net::HTTP.start(API_URI.hostname, API_URI.port, :use_ssl => true) do |h|
    h.request(req)
  end
  if res.is_a? Net::HTTPSuccess
    json = JSON.parse(res.body)
  else
    pp res
    json = Hash.new
    json["error"] = "Failed request"
  end
  return json
end

def item_page(session, type, id)
  json = get_api("/#{type}/#{id}")
  if json.has_key? "error"
    error_page(session, 404, "No #{type} with id #{id}")
    return
  end

  session.puts gen_http_header
  session.puts render_erb("item", {
    title: json['data']['attributes']['canonicalTitle'],
    type: type,
    attributes: json['data']['attributes']
  })
end

def search(session, type, q, page_offset)
  json = get_api("/#{type}?filter[text]=#{q}&page[limit]=15&page[offset]=" +
                 (page_offset * 15).to_s)
  session.puts gen_http_header()

  last_page = '0'
  has_last = false
  if json['links'] && json['links']['last']
    query = parse_url_query(URI(json['links']['last']).query)
    begin
      amount_offset = query["page[offset]"] || '0'
      offset = Float(amount_offset)
      if (page_offset * 15 < offset)
        has_last = true
        last_page = (offset / 15).ceil.to_s
      end
    rescue
      has_last = false
    end
  end

  has_next = json.has_key?("links") && json['links'].has_key?("next")

  session.puts(render_erb("search", {
    title: "Search results for #{q}",
    results_num: json['data'].size,
    type: type,
    q: URI.decode_www_form_component(q),
    page_offset: page_offset,
    has_last: has_last,
    has_next: has_next,
    last_page: last_page,
    json: json
  }))
end

# Parse the query into a hash
#
# for example: name=NAME&age=AGE
# will be returned as hash(String, String)
# { 'name' => 'NAME', 'age' => 'AGE' }
def parse_url_query(_query)
  ary = URI.decode_www_form(_query)
  return Hash[ary]
end

# create a 'browse' page for *type*.
#
# queries should be a Hash, it can be generated from `parse_url_query`.
def browse_page(session, type, query_string)
  query_hash = parse_url_query(query_string)

  # default values
  # the exception is pageless_query_hash
  page = 0
  subtype_arr = Array.new

  # default/fallback
  # the main reason the above variables are declared
  if query_string.empty? or !query_hash.has_key?("page")
    pageless_query_hash = URI.encode_www_form(query_hash)
    json = get_api("/#{type}?page[limit]=15&page[offset]=0")
  else
    begin
      page = Integer(query_hash['page'])
    rescue
      # no-op as page is zero already
    end
    query_hash.delete 'page'

    pageless_query_hash = URI.encode_www_form(query_hash)

    # for 'all' to work, filter[ageRating] needs to be removed.
    if query_hash["filter[ageRating]"] == "All"
      query_hash.delete "filter[ageRating]"
    end
    # same for all of these
    if query_hash["filter[season]"] == "all"
      query_hash.delete "filter[season]"
    end

    # multiple choice
    subtype_arr = query_get_all(URI.decode_www_form(query_string), "filter[subtype]")
    unless subtype_arr.size == 0
      query_hash["filter[subtype]"] = subtype_arr.join(',')
    end

    enc = URI.encode_www_form(query_hash)
    json = get_api("/#{type}?#{enc}&page[limit]=15&page[offset]=#{page * 15}", false)
  end

  # these are used for setting the values of the form.
  current_year = Time.new.year
  age_rating = query_hash["filter[ageRating]"] || "All"
  season = query_hash["filter[season]"] || "All"

  if type == "anime"
    filter_year = query_hash["filter[seasonYear]"] || current_year
  else
    filter_year = query_hash["filter[year]"] || current_year
  end

  # pagination
  if json.has_key?("links")
    has_next = json['links'].has_key?("next")
    has_last = json['links'].has_key?("last")
  else
    has_next = false
    has_last = false
  end

  if has_last
    query = parse_url_query(URI(json['links']['last']).query)
    begin
      amount_offset = query["page[offset]"] || '0'
      offset = Float(amount_offset)
      if (page * 15 < offset)
        has_last = true
        last_page = (offset / 15).ceil.to_s
      else
        # at the last page
        has_last = false
      end
    rescue
      has_last = false
    end
  else
    last_page = '0'
  end

  session.puts gen_http_header()
  session.puts(render_erb("browse", {
    age_rating: age_rating,
    current_year: current_year,
    has_next: has_next,
    has_last: has_last,
    json: json,
    last_page: last_page,
    page: page,
    pageless_queries: pageless_query_hash,
    season: season,
    subtypes: subtype_arr,
    filter_year: filter_year,
    type: type
  }))
end

def get(session, path)
  case path
  when "/"
    # fetch trending anime and manga
    ani = get_api("/trending/anime")
    man = get_api("/trending/manga")
    session.puts gen_http_header(200, "text/html", true)
    session.puts render_erb("index", { anime: ani, manga: man })
  when "/css/main.css"
    begin
      f = File.open("./css/main.css")
      session.puts gen_http_header(200, "text/css", true)
      session.puts f.read
      f.close
    rescue
      session.print(gen_http_header 404, 'text/plain')
      session.puts "File not found"
    end
  when /\/search?.*/
    uri = URI(path)
    query = parse_url_query(uri.query)
    if query['q'] && query['type']
      if query['q'].empty? || query['type'].empty?
        error_page(session, 400, "Either no query or no type was provided.")
        return
      end
      if query['page']
        begin
          offset = Integer(query['page'])
        rescue ArgumentError
          offset = 0
        end
      else
        offset = 0
      end
      offset = 0 if offset < 0
      search(session, query['type'], query['q'], offset)
    else
      error_page(session, 400,
        "The request was missing a required parameter ('q' or 'type')")
    end
  when /\/(anime|manga)\/[0-9]+/
    match = path.match(/\/(?<type>anime|manga)\/(?<id>[0-9]+)/)
    type = match[:type]
    id = match[:id]
    item_page(session, type, id)
  when /\/(anime|manga)/
    match = path.match(/(?<type>anime|manga)/)
    uri = URI(path)
    query_string = uri.query || ""
    type = match[:type]
    browse_page(session, type, query_string)
  else
    error_page(session, 404, "The requested page could not be found.")
  end
end

def error_page(session, status, msg)
  session.puts gen_http_header(status, "text/html", true)
  session.puts render_erb("error", {
    status: status,
    msg: msg
  })
end

# main program begins here

begin
  SERVER = TCPServer.open(PORT)
rescue Errno::EADDRINUSE
  puts "Could not bind to port #{PORT} as it is already in use."
  exit 1
end

begin
  puts "crus is listening on port #{PORT}, (http://localhost:#{PORT})"
  while session = SERVER.accept
    req = parse_req(session.gets)
    case req["method"]
    when "GET"
      puts "GET #{req['path']}"
      get(session, req["path"])
    else
      puts "#{req['method']} #{req['path']}"
      error_page(session, 405, "#{req['method']} requests are not supported")
    end
    session.close
  end
rescue Interrupt
  # newline from ^C
  puts
  puts 'Closing crus...'
  session.close if session.class == TCPSocket
end
